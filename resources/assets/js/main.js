var Vue = require('vue');
var VueRouter = require('vue-router')

Vue.use(require('vue-resource'));
Vue.use(VueRouter);

var App = Vue.extend({})
var router = new VueRouter()

//Component classes worden geregistreerd
import GMap from './components/Map.vue';
import Post from './components/Post.vue';
import Tickets from './components/Tickets.vue';

//Routing worden geregistreerd
router.map({
    '/': {
        component: GMap
    },
    '/post': {
        component: Post
    },
    '/tickets': {
        component: Tickets
    }
})

//Starten van de app
router.start(App, '#app')

