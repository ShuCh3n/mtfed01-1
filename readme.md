## MT FED01 1 Opdracht

Eindopdracht voor Javascript.

Hogeschool Rotterdam 2016

De gebruiker krijgt een kaart te zien waarbij hij een interesse in kan voeren. De ingevoerde interesse word vervolgens op socialmedia opgehaald. De plekken worden vervolgens getoont op de kaart. Zo kan de gebruiker zien waar zijn interesse op de wereld meeste voorkomt.

De gebruiker kan de post in details bekijken. De gebruiker kan vervolgens een vliegticket boek naar dat land.

Het project zal gemakent worden in Vue.js met behulp van Gulp. De interesse post worden opgehaald in de verzamelde database. Dit zal gebeuren door middel van een REST call naar een server. Het interesse punten worden vervolgens getoont op de kaart van Google Maps.

Het is mogelijk om voor de gebruiker in details de post te bekijken. Het bericht van de afbeelding worden opgehaald en dit wordt vervolgens getoonnt.

Wanneer de gebruiker een land aan klikt is het mogelijk om naar een vliegticket te zoeken. De gebruiker geeft op de datums op. Vervolgens worden data opgehaald door de API van Skyscanner
